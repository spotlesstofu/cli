.. _glab_variable_get:

glab variable get
-----------------

get a project or group variable

Synopsis
~~~~~~~~


get a project or group variable

::

  glab variable get <key> [flags]

Examples
~~~~~~~~

::

  $ glab variable get VAR_KEY
           $ glab variable get -g GROUP VAR_KEY
  

Options
~~~~~~~

::

  -g, --group string   Get variable for a group

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

      --help              Show help for command
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or `GROUP/NAMESPACE/REPO` format or full URL or git URL

